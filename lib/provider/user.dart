import 'package:flutter/foundation.dart';

class UserProvider extends ChangeNotifier{
  int? Id ;
  String? Name;
  int? Brand;
  Future saveUser(int a,String b) async {
    Id = a ;
    Name = b;
    notifyListeners();
  }


  Future saveBrand(int a) async {
    Brand = a ;
    notifyListeners();
  }

}
