import 'package:flutter/material.dart';
class Carditem extends StatefulWidget {
  final String name;
  final String date;
  final String img;
  const Carditem({super.key, required this.name, required this.date, required this.img});

  @override
  State<Carditem> createState() => _CardState();
}

class _CardState extends State<Carditem> {
  @override
  Widget build(BuildContext context) {
    return  Container(
      width: 300,
      margin: EdgeInsets.only(right: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
        border: Border.all(color: Colors.white),
      ),
      child: InkWell(
        onTap: (){},
        child: Padding(
          padding: EdgeInsets.all(12.0),
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 300,
                  height: 150,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: Colors.grey.shade200,
                      image: DecorationImage(
                        image: AssetImage(widget.img),
                        fit: BoxFit.cover,
                      )
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  widget.name,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 15
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(height: 6,),
                Row(
                  children: [
                    //Icon(Icons.location_on,color: Colors.grey,),
                    Text(
                      widget.date,
                      style: TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 16.0,
                          color: Colors.grey
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
