import 'package:flutter/material.dart';
import 'package:water/Widget/card.dart';
class Recommendation extends StatefulWidget {
  const Recommendation({super.key});

  @override
  State<Recommendation> createState() => _RecommendationState();
}

class _RecommendationState extends State<Recommendation> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(left: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Thông báo dịch vụ",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'Roboto',
                  ),
                ),
                TextButton(onPressed: (){}, child: Text("See All"))
              ],
            ),
          ),
          SizedBox(height: 5,),
          Container(
            height: 250,
            width: double.infinity,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                Carditem(name: "Thông báo mở kênh thanh toán Momo, hướng", date: "02-02-2024", img: "asset/image/nuoc2.jpg"),
                Carditem(name: "Bảng giá nước thay đổi theo quý...", date: "02-08-2024", img: "asset/image/nuoc.jpg"),
                Carditem(name: "Nâng cấp dịch vụ chăm sóc khác hàng", date: "02-02-2024", img: "asset/image/nuoc1.jpg")
              ],
            ),
          )
        ],
      ),
    );
  }
}
