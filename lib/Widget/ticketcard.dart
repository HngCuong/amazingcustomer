import 'package:flutter/material.dart';
import 'package:icons_plus/icons_plus.dart';
import 'package:water/models/task.dart';
class TicketCard extends StatefulWidget {
  final TaskModel op;
  const TicketCard({super.key, required this.op});

  @override
  State<TicketCard> createState() => _TicketCardState();
}

class _TicketCardState extends State<TicketCard> {

  String formatDate(String inputDate) {
    List<String> parts = inputDate.split('T');
    String datePart = parts[0];
    List<String> dateParts = datePart.split('-');
    int year = int.parse(dateParts[0]);
    int month = int.parse(dateParts[1]);
    int day = int.parse(dateParts[2]);
    String formattedDate = '$day/${month.toString().padLeft(2, '0')}/$year';
    return formattedDate;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
              Radius.circular(30)
          )// Màu nền của container
      ),

      margin: EdgeInsets.only(bottom: 10),
      height: 101,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              SizedBox(width: 20,),
              Icon(Iconsax.ship,size: 20,),
              SizedBox(width:10,),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Trạng Thái: " + widget.op.ticketStatusId! ),
                    Text(widget.op.title!),
                  ],
                ),
              ),
              IconButton(onPressed: (){}, icon: Icon(Iconsax.arrow_right,size: 30,))
            ],
          ),
          SizedBox(height: 5,),
          Row(
            children: [
              SizedBox(width: 20,),
              Expanded(
                child: Row(
                  children: [
                    Icon(Icons.access_time_outlined,size: 20,),
                    SizedBox(width:10,),
                    Expanded(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Ngày bắt đầu"),
                          Text(formatDate(widget.op.createdDate!))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    Icon(Icons.access_time_outlined,size: 20,),
                    SizedBox(width:10,),
                    Expanded(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Ngày kết thúc"),
                          Text(formatDate(widget.op.createdDate!))
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SizedBox(height: 8,)
        ],
      ),

    );
  }
}
