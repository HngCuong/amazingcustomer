class Schedule {
  int? id;
  int? customerId;
  int? staffId;
  String? tittle;
  String? status;
  String? note;
  String? createdDate;
  String? meetTime;
  String? lastEditedTime;

  Schedule(
      {this.id,
        this.customerId,
        this.staffId,
        this.tittle,
        this.status,
        this.note,
        this.createdDate,
        this.meetTime,
        this.lastEditedTime});

  Schedule.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customerId = json['customerId'];
    staffId = json['staffId'];
    tittle = json['tittle'];
    status = json['status'];
    note = json['note'];
    createdDate = json['createdDate'];
    meetTime = json['meetTime'];
    lastEditedTime = json['lastEditedTime'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['customerId'] = this.customerId;
    data['staffId'] = this.staffId;
    data['tittle'] = this.tittle;
    data['status'] = this.status;
    data['note'] = this.note;
    data['createdDate'] = this.createdDate;
    data['meetTime'] = this.meetTime;
    data['lastEditedTime'] = this.lastEditedTime;
    return data;
  }
}