class TaskModel {
  // String id;
  // String title;
  // String description;
  // DateTime? startDateTime;
  // DateTime? stopDateTime;
  // bool completed;
  int? id;
  int? branchId;
  String? note;
  CustomerId? customerId;
  String? ticketTypeId;
  String? levelId;
  String? ticketStatusId;
  String? createdDate;
  String? code;
  String? title;
  String? createdBy;
  int? assignedUserId;
  TaskModel({
    // required this.id,
    // required this.title,
    // required this.description,
    // required this.startDateTime,
    // required this.stopDateTime,
    // this.completed = false,
    this.id,
    this.createdBy,
    this.branchId,
    this.note,
    this.customerId,
    this.ticketTypeId,
    this.levelId,
    this.ticketStatusId,
    this.createdDate,
    this.code,
    this.title,
    this.assignedUserId
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['assignedUserId'] = this.assignedUserId;
    data['id'] = this.id;
    data['createdBy'] = this.createdBy;
    data['branchId'] = this.branchId;
    data['note'] = this.note;
    if (this.customerId != null) {
      data['customerId'] = this.customerId!.toJson();
    }
    data['ticketTypeId'] = this.ticketTypeId;
    data['levelId'] = this.levelId;
    data['ticketStatusId'] = this.ticketStatusId;
    data['createdDate'] = this.createdDate;
    data['code'] = this.code;
    data['title'] = this.title;
    return data;
  }

  TaskModel.fromJson(Map<String, dynamic> json) {
    assignedUserId = json['assignedUserId'];
    createdBy = json['createdBy'];
    id = json['id'];
    branchId = json['branchId'];
    note = json['note'];
    customerId = json['customerId'] != null
        ? new CustomerId.fromJson(json['customerId'])
        : null;
    ticketTypeId = json['ticketTypeId'];
    levelId = json['levelId'];
    ticketStatusId = json['ticketStatusId'];
    createdDate = json['createdDate'];
    code = json['code'];
    title = json['title'];
  }
}





class CustomerId {
  int? id;
  int? customerLevelId;
  int? sourceId;
  int? branchId;
  String? lastName;
  String? firstName;
  String? phoneNumber;
  String? status;
  String? gender;
  String? address;
  String? dayOfBirth;
  String? dateCreated;
  String? lastEditedTime;
  String? name;
  String? email;

  CustomerId(
      {this.id,
        this.customerLevelId,
        this.sourceId,
        this.branchId,
        this.lastName,
        this.firstName,
        this.phoneNumber,
        this.status,
        this.gender,
        this.address,
        this.dayOfBirth,
        this.dateCreated,
        this.lastEditedTime,
        this.name,
        this.email});

  CustomerId.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customerLevelId = json['customerLevelId'];
    sourceId = json['sourceId'];
    branchId = json['branchId'];
    lastName = json['lastName'];
    firstName = json['firstName'];
    phoneNumber = json['phoneNumber'];
    status = json['status'];
    gender = json['gender'];
    address = json['address'];
    dayOfBirth = json['dayOfBirth'];
    dateCreated = json['dateCreated'];
    lastEditedTime = json['lastEditedTime'];
    name = json['name'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['customerLevelId'] = this.customerLevelId;
    data['sourceId'] = this.sourceId;
    data['branchId'] = this.branchId;
    data['lastName'] = this.lastName;
    data['firstName'] = this.firstName;
    data['phoneNumber'] = this.phoneNumber;
    data['status'] = this.status;
    data['gender'] = this.gender;
    data['address'] = this.address;
    data['dayOfBirth'] = this.dayOfBirth;
    data['dateCreated'] = this.dateCreated;
    data['lastEditedTime'] = this.lastEditedTime;
    data['name'] = this.name;
    data['email'] = this.email;
    return data;
  }
}
