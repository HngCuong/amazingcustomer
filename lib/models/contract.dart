class ContractModel {
  int? id;
  CustomerId? customerId;
  String? contractType;
  String? termsAndConditions;
  String? timeStart;
  String? timeEnd;
  String? lastEditedTime;
  String? code;
  String? title;
  String? address;
  String? callNumber;
  int? branchId;

  ContractModel(
      {this.id,
        this.customerId,
        this.contractType,
        this.termsAndConditions,
        this.timeStart,
        this.timeEnd,
        this.lastEditedTime,
        this.code,
        this.title,
        this.address,
        this.callNumber,
        this.branchId});

  ContractModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customerId = json['customerId'] != null
        ? new CustomerId.fromJson(json['customerId'])
        : null;
    contractType = json['contractType'];
    termsAndConditions = json['termsAndConditions'];
    timeStart = json['timeStart'];
    timeEnd = json['timeEnd'];
    lastEditedTime = json['lastEditedTime'];
    code = json['code'];
    title = json['title'];
    address = json['address'];
    callNumber = json['callNumber'];
    branchId = json['branchId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.customerId != null) {
      data['customerId'] = this.customerId!.toJson();
    }
    data['contractType'] = this.contractType;
    data['termsAndConditions'] = this.termsAndConditions;
    data['timeStart'] = this.timeStart;
    data['timeEnd'] = this.timeEnd;
    data['lastEditedTime'] = this.lastEditedTime;
    data['code'] = this.code;
    data['title'] = this.title;
    data['address'] = this.address;
    data['callNumber'] = this.callNumber;
    data['branchId'] = this.branchId;
    return data;
  }
}

class CustomerId {
  int? id;
  int? customerLevelId;
  int? sourceId;
  int? branchId;
  String? lastName;
  String? firstName;
  String? phoneNumber;
  String? status;
  String? gender;
  String? address;
  String? dayOfBirth;
  String? dateCreated;
  String? lastEditedTime;
  String? name;
  String? email;

  CustomerId(
      {this.id,
        this.customerLevelId,
        this.sourceId,
        this.branchId,
        this.lastName,
        this.firstName,
        this.phoneNumber,
        this.status,
        this.gender,
        this.address,
        this.dayOfBirth,
        this.dateCreated,
        this.lastEditedTime,
        this.name,
        this.email});

  CustomerId.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    customerLevelId = json['customerLevelId'];
    sourceId = json['sourceId'];
    branchId = json['branchId'];
    lastName = json['lastName'];
    firstName = json['firstName'];
    phoneNumber = json['phoneNumber'];
    status = json['status'];
    gender = json['gender'];
    address = json['address'];
    dayOfBirth = json['dayOfBirth'];
    dateCreated = json['dateCreated'];
    lastEditedTime = json['lastEditedTime'];
    name = json['name'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['customerLevelId'] = this.customerLevelId;
    data['sourceId'] = this.sourceId;
    data['branchId'] = this.branchId;
    data['lastName'] = this.lastName;
    data['firstName'] = this.firstName;
    data['phoneNumber'] = this.phoneNumber;
    data['status'] = this.status;
    data['gender'] = this.gender;
    data['address'] = this.address;
    data['dayOfBirth'] = this.dayOfBirth;
    data['dateCreated'] = this.dateCreated;
    data['lastEditedTime'] = this.lastEditedTime;
    data['name'] = this.name;
    data['email'] = this.email;
    return data;
  }
}