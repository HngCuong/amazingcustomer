class Request {
  int? branchId;
  String? note;
  int? customerId;
  int? ticketTypeId;
  int? levelId;
  int? ticketStatusId;
  String? createdDate;
  String? title;

  Request(
      {this.branchId,
        this.note,
        this.customerId,
        this.ticketTypeId,
        this.levelId,
        this.ticketStatusId,
        this.createdDate,
        this.title});

  Request.fromJson(Map<String, dynamic> json) {
    branchId = json['branchId'];
    note = json['note'];
    customerId = json['customerId'];
    ticketTypeId = json['ticketTypeId'];
    levelId = json['levelId'];
    ticketStatusId = json['ticketStatusId'];
    createdDate = json['createdDate'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['branchId'] = this.branchId;
    data['note'] = this.note;
    data['customerId'] = this.customerId;
    data['ticketTypeId'] = this.ticketTypeId;
    data['levelId'] = this.levelId;
    data['ticketStatusId'] = this.ticketStatusId;
    data['createdDate'] = this.createdDate;
    data['title'] = this.title;
    return data;
  }
}