class LogRequest {
  int? ticketId;
  String? note;
  String? createdDate;
  String? code;
  String? imgUrl;

  LogRequest(
      {
        this.ticketId,
        this.note,
        this.createdDate,
        this.code,
        this.imgUrl});

  LogRequest.fromJson(Map<String, dynamic> json) {

    ticketId = json['ticketId'];
    note = json['note'];
    createdDate = json['createdDate'];
    code = json['code'];
    imgUrl = json['imgUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ticketId'] = this.ticketId;
    data['note'] = this.note;
    data['createdDate'] = this.createdDate;
    data['code'] = this.code;
    data['imgUrl'] = this.imgUrl;
    return data;
  }
}