class Ticket {
  int? id;
  int? branchId;
  String? note;
  int? customerId;
  int? ticketTypeId;
  int? levelId;
  int? ticketStatusId;
  String? createdDate;
  String? createdBy;
  int? assignedUserId;
  String? code;
  String? title;

  Ticket(
      {this.id,
        this.assignedUserId,
        this.createdBy,
        this.branchId,
        this.note,
        this.customerId,
        this.ticketTypeId,
        this.levelId,
        this.ticketStatusId,
        this.createdDate,
        this.code,
        this.title,
       });

  Ticket.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    assignedUserId = json['assignedUserId'];
    createdBy = json['createdBy'];
    branchId = json['branchId'];
    note = json['note'];
    customerId = json['customerId'];
    ticketTypeId = json['ticketTypeId'];
    levelId = json['levelId'];
    ticketStatusId = json['ticketStatusId'];
    createdDate = json['createdDate'];
    code = json['code'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['assignedUserId'] = this.assignedUserId;
    data['createdBy'] = this.createdBy;
    data['branchId'] = this.branchId;
    data['note'] = this.note;
    data['customerId'] = this.customerId;
    data['ticketTypeId'] = this.ticketTypeId;
    data['levelId'] = this.levelId;
    data['ticketStatusId'] = this.ticketStatusId;
    data['createdDate'] = this.createdDate;
    data['code'] = this.code;
    data['title'] = this.title;
    return data;
  }
}

class CustomerId {
  int? id;
  int? customerLevelId;
  int? sourceId;
  int? branchId;
  String? lastName;
  String? firstName;
  String? phoneNumber;
  String? status;
  String? gender;
  String? address;
  String? dayOfBirth;
  String? dateCreated;
  String? lastEditedTime;
  String? createdBy;
  String? name;
  String? email;

  CustomerId(
      {this.id,
        this.createdBy,
        this.customerLevelId,
        this.sourceId,
        this.branchId,
        this.lastName,
        this.firstName,
        this.phoneNumber,
        this.status,
        this.gender,
        this.address,
        this.dayOfBirth,
        this.dateCreated,
        this.lastEditedTime,
        this.name,
        this.email});

  CustomerId.fromJson(Map<String, dynamic> json) {
    createdBy = json['createdBy'];
    id = json['id'];
    customerLevelId = json['customerLevelId'];
    sourceId = json['sourceId'];
    branchId = json['branchId'];
    lastName = json['lastName'];
    firstName = json['firstName'];
    phoneNumber = json['phoneNumber'];
    status = json['status'];
    gender = json['gender'];
    address = json['address'];
    dayOfBirth = json['dayOfBirth'];
    dateCreated = json['dateCreated'];
    lastEditedTime = json['lastEditedTime'];
    name = json['name'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdBy'] = this.createdBy;
    data['id'] = this.id;
    data['customerLevelId'] = this.customerLevelId;
    data['sourceId'] = this.sourceId;
    data['branchId'] = this.branchId;
    data['lastName'] = this.lastName;
    data['firstName'] = this.firstName;
    data['phoneNumber'] = this.phoneNumber;
    data['status'] = this.status;
    data['gender'] = this.gender;
    data['address'] = this.address;
    data['dayOfBirth'] = this.dayOfBirth;
    data['dateCreated'] = this.dateCreated;
    data['lastEditedTime'] = this.lastEditedTime;
    data['name'] = this.name;
    data['email'] = this.email;
    return data;
  }
}