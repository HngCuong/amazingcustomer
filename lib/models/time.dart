class TimeSchedule {
  int? day;
  int? hour;
  int? minute;
  int? second;

  TimeSchedule(
      {
        this.day,
        this.hour,
        this.minute,
        this.second,
      });
}