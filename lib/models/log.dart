class Log {
  int? id;
  int? ticketId;
  String? note;
  String? createdDate;
  String? code;
  String? imgUrl;

  Log(
      {this.id,
        this.ticketId,
        this.note,
        this.createdDate,
        this.code,
        this.imgUrl});

  Log.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    ticketId = json['ticketId'];
    note = json['note'];
    createdDate = json['createdDate'];
    code = json['code'];
    imgUrl = json['imgUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['ticketId'] = this.ticketId;
    data['note'] = this.note;
    data['createdDate'] = this.createdDate;
    data['code'] = this.code;
    data['imgUrl'] = this.imgUrl;
    return data;
  }
}