import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:water/models/account.dart';
import 'package:water/models/contract.dart';
import 'package:water/models/log.dart';
import 'package:water/models/logrequest.dart';
import 'package:water/models/request.dart';
import 'package:water/models/schedule.dart';
import 'package:water/models/task.dart';
import 'package:water/models/ticket.dart';
import 'package:water/provider/user.dart';

//Api Login
Future<bool> createProduct(BuildContext context, Account a) async {
  Map<String, String> headers = {
    "Content-Type": "application/json",
  };
  final uri = Uri.parse("https://10.20.3.130:45455/login_mobile");
  final response = await http.post(uri, body: jsonEncode(a.toJson()), headers: headers );
  if(response.statusCode == 200){
    Map<String, dynamic> data = json.decode(response.body);
    int password = data['id'];
    String name = data['name'];
    final provider = Provider.of<UserProvider>(context, listen: false);
    provider.saveUser(password, name);
    return true;
  }
  else{
    return false;
  }
}

Future<List<TaskModel>> fetchTicket(BuildContext context) async{
  final provider = Provider.of<UserProvider>(context, listen: false);
  final response = await http.get(Uri.parse('https://10.20.3.130:45455/api/ticket/CustomerId/${provider.Id}'));
  if(response.statusCode == 200){
    print('Chạy api lay Ticket Thành Công');
    return compute(parseTicket, response.body);
  }
  else{
    throw Exception('Request API Error' + response.statusCode.toString());
  }
}

List<TaskModel> parseTicket(String responseBody){
  print('Chạy api parseTicket');
  var list = jsonDecode(responseBody) as List<dynamic>;
  print('Chạy api parseTicket 2');
  List<TaskModel> ticket = list.map((model) => TaskModel.fromJson(model)).toList();
  print('Chạy api parseTicket 3');
  return ticket;
}

//Api get Log By TicketID
Future<List<Log>> fetchLog(int id) async{
  final response = await http.get(Uri.parse('https://10.20.3.130:45455/api/log/ticketId?ticketId=${id}'));
  if(response.statusCode == 200){
    print('Chạy api lay Log Thành Công');
    return compute(parseLog, response.body);
  }
  else{
    throw Exception('Request API Error' + response.statusCode.toString());
  }
}

List<Log> parseLog(String responseBody){
  print('Chạy api parseTicket');
  var list = jsonDecode(responseBody) as List<dynamic>;
  print('Chạy api parseTicket 2');
  List<Log> ticket = list.map((model) => Log.fromJson(model)).toList();
  print('Chạy api parseTicket 3');
  return ticket;
}

//Api thay đổi status
Future<bool> changeStatus(Ticket a) async {
  Map<String, String> headers = {
    "Content-Type": "application/json",
  };
  final uri = Uri.parse("https://10.20.3.130:45455/api/ticket");
  final response = await http.put(uri, body: jsonEncode(a.toJson()), headers: headers );
  print(jsonEncode(a.toJson()));
  if(response.statusCode == 200){
    print("Login Success");
    return true;
  }
  else{
    return false;
  }
}

//Tạo Logs
Future<bool> createRequest(Request a) async {
  print('Có chạy createlog');
  Map<String, String> headers = {
    "Content-Type": "application/json",
  };
  final uri = Uri.parse("https://10.20.3.130:45455/ticketbycustomer");
  final response = await http.post(uri, body: jsonEncode(a.toJson()), headers: headers );
  print(jsonEncode(a.toJson()));
  if(response.statusCode == 200){
    print("Login Success");
    return true;
  }
  else{
    return false;
  }
}

//Api get Contract
Future<ContractModel> fetchContract(BuildContext context) async{
  final provider = Provider.of<UserProvider>(context, listen: false);
  final response = await http.get(Uri.parse('https://10.20.3.130:45455/api/contract/customerId?customerId=${provider.Id}'));
  if(response.statusCode == 200){
    Map<String, dynamic> data = json.decode(response.body);
    int branchId = data['branchId'];
    provider.saveBrand(branchId);
    return compute(parseContract, response.body);
  }
  else{
    throw Exception('Request API Error' + response.statusCode.toString());
  }
}

ContractModel parseContract(String responseBody){
  var list = jsonDecode(responseBody) ;

  return ContractModel.fromJson(list);
}

//Api get Log By TicketID
Future<Schedule> fetchSchedule(BuildContext context) async{
  final provider = Provider.of<UserProvider>(context, listen: false);
  final response = await http.get(Uri.parse('https://10.20.3.130:45455/api/schedule/customer/${provider.Id}'));
  if(response.statusCode == 200){
    print('Chạy api lay Log Thành Công');
    return compute(parseSchedule, response.body);
  }
  else{
    throw Exception('Request API Error' + response.statusCode.toString());
  }
}

Schedule parseSchedule(String responseBody) {
  var data = jsonDecode(responseBody);
  return Schedule.fromJson(data);
}