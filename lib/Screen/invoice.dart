import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:water/Widget/chart.dart';
class Invoice extends StatefulWidget {
  const Invoice({super.key});

  @override
  State<Invoice> createState() => _InvoiceState();
}

class _InvoiceState extends State<Invoice> {
  List<String> list =  ["10/2023","11/2023","12/2023","1/2024","2/2024","3/2024","4/2024","5/2024"];
  List<String> list1 = ["5L","5L","6L","3L","5.4L","6.1L","7.2L","5.1L"];
  List<String> list2 = ["145.00","145.00","98.00","162.00","159.00","168.00","189.00","151.00"];
  List<String> list3 = ["Đã thanh toán","Đã thanh toán","Đã thanh toán","Đã thanh toán","Đã thanh toán","Đã thanh toán","Đã thanh toán","Đã thanh toán"];
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: Color(0xFF2CB4DC)
      ),
      child: Column(
        children: [
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    Container(
                        margin: EdgeInsets.only(left: 15),
                        child: Center(
                            // child: IconButton(
                            //     color: Colors.white,
                            //     onPressed: () {},
                            //     icon: Icon(Icons.arrow_back_ios)
                            // )
                        )
                    )
                    ,
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Center(
                        child: Text(
                          "Quản lý hóa đơn",
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.normal,
                            color: Colors.white,
                            fontFamily: 'Roboto',
                          ),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height - 150.6,
            decoration: BoxDecoration(
              color: Colors.grey.shade100, // Màu nền của container
            ),
            child: Column(
              children: [
                Container(
                  child: LineChartSample2(),
                ),
                SizedBox(height: 10,),
                Container(
                  width: 700,
                  height: 450,
                  child: Container(
                    child: DataTable2(
                        columnSpacing: 12,
                        horizontalMargin: 12,
                        minWidth: 100,
                        columns: [
                          DataColumn2(
                            label: Text('Kỳ',style: TextStyle(color: Colors.blue,fontSize: 18)),
                            size: ColumnSize.S,
                          ),
                          DataColumn(
                            label: Text('Tiêu Thụ',style: TextStyle(color: Colors.blue,fontSize: 18)),
                          ),
                          DataColumn(
                            label: Text('Tổng Tiền',style: TextStyle(color: Colors.blue,fontSize: 18)),
                          ),
                          DataColumn(
                            label: Text('Thanh Toán',style: TextStyle(color: Colors.blue,fontSize: 18)),
                          ),

                        ],
                        rows: List<DataRow>.generate(
                            8,
                                (index) => DataRow(cells: [
                              DataCell(Text(list[index])),
                              DataCell(Text(list1[index])),
                              DataCell(Text(list2[index])),
                              DataCell(Text(list3[index],style: TextStyle(color: Colors.blue),)),
                            ]))),
                  ),
                )
              ],
            ),
          )

        ],
      ),
    );
  }
}
