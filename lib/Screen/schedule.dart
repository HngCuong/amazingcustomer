import 'package:flutter/material.dart';
import 'package:flutter_timer_countdown/flutter_timer_countdown.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:rive/rive.dart';
import 'package:water/api/apis.dart';
import 'package:water/models/schedule.dart';
import 'package:water/models/time.dart';
class ScheduleScreen extends StatefulWidget {
  const ScheduleScreen({super.key});

  @override
  State<ScheduleScreen> createState() => _ScheduleScreenState();
}

class _ScheduleScreenState extends State<ScheduleScreen> {
  Schedule schedule = new Schedule();
  TimeSchedule time = new TimeSchedule();
  @override
  void initState() {
    super.initState();
    fetchSchedule(context).then((value) => {
      setState(() {
        schedule = value;
        DateTime startDate = DateTime.parse(value.meetTime!);
        DateTime now = DateTime.now();
        Duration difference = startDate.difference(now);
        int days = difference.inDays;
        int hours = difference.inHours - (days * 24);
        int minutes = difference.inMinutes - (days * 24 * 60) - (hours * 60);
        int seconds = difference.inSeconds - (days * 24 * 60 * 60) - (hours * 60 * 60) - (minutes * 60);
        time = new TimeSchedule(day: days,hour: hours ,minute: minutes,second: seconds);
        print(time.second);
        print(time.hour);
        print(time.minute);
        print(time.second);
      })
    });
  }
  String formatDate(String inputDate) {
    List<String> parts = inputDate.split('T');
    String datePart = parts[0];
    List<String> dateParts = datePart.split('-');
    int year = int.parse(dateParts[0]);
    int month = int.parse(dateParts[1]);
    int day = int.parse(dateParts[2]);
    String formattedDate = '$day/${month.toString().padLeft(2, '0')}/$year';
    return formattedDate;
  }


  @override
  Widget build(BuildContext context) {
    return
      schedule.meetTime != null ?
      Container(
      child: Column(
        children: [
          Container(
            height: 400,
            child: RiveAnimation.asset(
              'asset/image/notificacion_2.riv',
            ),
          ),
          Text("Bạn đang có 1 Lịch hẹn",
            style: TextStyle(
            color: Colors.black,
            fontSize: 30.0,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.normal,
            fontFamily: 'Roboto',
            letterSpacing: 1.0,
          ),
          ),
          SizedBox(height: 30,),
          Text("Bạn có 1 lịch hẹn " + formatDate(schedule.meetTime!),
            style: TextStyle(
              color: Colors.black,
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.normal,
              fontFamily: 'Roboto',
              letterSpacing: 1.0,
            ),
          ),
          SizedBox(height: 15,),
          Text("Thời gian còn",
            style: TextStyle(
              color: Colors.black,
              fontSize: 14.0,
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.normal,
              fontFamily: 'Roboto',
              letterSpacing: 1.0,
            ),
          ),
          SizedBox(height: 15,),
          TimerCountdown(
            colonsTextStyle: TextStyle(
              fontSize: 14.0,
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.normal,
              fontFamily: 'Roboto',
            ),
            timeTextStyle: TextStyle(
              fontSize: 14.0,
              fontWeight: FontWeight.bold,
              fontStyle: FontStyle.normal,
              fontFamily: 'Roboto',
            ),
            format: CountDownTimerFormat.daysHoursMinutesSeconds,
            endTime: DateTime.now().add(
              Duration(
                days: time.day!,
                hours: time.hour!,
                minutes: time.minute!,
                seconds: time.second!,
              ),
            ),
            onEnd: () {
              print("Timer finished");
            },
          ),
        ],
      ),
    ) :
      Scaffold(
          body: Center(
            child: LoadingAnimationWidget.staggeredDotsWave(
              color: Colors.black,
              size: 200,
            ),
          ));
  }
}
