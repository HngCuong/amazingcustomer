import 'package:flutter/material.dart';
import 'package:water/Screen/invoice.dart';
import 'package:water/Screen/signup_screen.dart';
import 'package:water/screen/signin_screen.dart';
import 'package:water/theme/theme.dart';
import 'package:water/Widget/custom_scaffold.dart';
import 'package:water/Widget/welcome_button.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      child: Column(
        children: [
          Flexible(
              flex: 8,
              child: Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 0,
                  horizontal: 40.0,
                ),
                child: Center(
                  child: Container(
                    margin: EdgeInsets.only(top: 100) ,
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: const TextSpan(
                        children: [
                          TextSpan(
                              text: 'Welcome Back!\n',
                              style: TextStyle(
                                fontSize: 45.0,
                                fontWeight: FontWeight.w600,
                              )),
                          TextSpan(
                              text:
                                  '\nĐăng nhập để sử dụng và tra cứu thông tin',
                              style: TextStyle(
                                fontSize: 20,
                                // height: 0,
                              ))
                        ],
                      ),
                    ),
                  ),
                ),
              )),
          Flexible(
            flex: 1,
            child: Align(
              alignment: Alignment.bottomRight,
              child: Row(
                children: [
                  const Expanded(
                    child: WelcomeButton(
                      buttonText: 'Đăng Kí',
                      onTap: SignUpScreen(),
                      color: Colors.transparent,
                      textColor: Colors.white,
                    ),
                  ),
                  Expanded(
                    child: WelcomeButton(
                      buttonText: 'Đăng Nhập',
                      onTap: SignInScreen(),
                      color: Colors.white,
                      textColor: lightColorScheme.primary,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
