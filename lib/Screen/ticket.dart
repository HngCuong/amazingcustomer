import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:water/Screen/form.dart';
import 'package:water/Widget/chart.dart';
import 'package:water/Widget/ticketcard.dart';
import 'package:water/api/apis.dart';
import 'package:water/models/task.dart';

class Ticket extends StatefulWidget {
  const Ticket({super.key});

  @override
  State<Ticket> createState() => _TicketState();
}

class _TicketState extends State<Ticket> {
  List<TaskModel> list = [];
  int flag = 0;
  @override
  void initState() {
    super.initState();
    fetchTicket(context).then((value) => {
      setState(() {
        list = value;
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    return list.length > 0 ? Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          color: Color(0xFF2CB4DC)
      ),
      child: flag  == 0 ? Column(
        children: [
          Container(
            height: 150,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
                Container(
                  height: 150,
                  child: Stack(
                    children: [
                      Positioned(
                        child: Container(
                          height: 75,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(20.0),
                                bottomRight:
                                    Radius.circular(20.0)), // Độ bo tròn góc
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        child: Container(
                          height: 65,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: Colors.grey.shade100,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20.0),
                                topRight:
                                Radius.circular(20.0)), // Độ bo tròn góc
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 35,
                        top: 35,
                        left: 70,
                        right: 70,
                        child: InkWell(
                          onTap: () {
                           setState(() {
                             flag = 1;
                           });
                          },
                          child: Container(
                            alignment: Alignment.center,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(Radius.circular(
                                    30)) // Màu nền của container
                                ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 30,
                                  height: 30,
                                  margin: EdgeInsets.only(left: 20),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Colors.black,
                                        width: 2.0, // Màu của viền
                                      ),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(
                                              30)) // Màu nền của container
                                      ),
                                  child: Icon(Icons.add),
                                ),
                                Container(
                                    margin: EdgeInsets.only(right: 20),
                                    child: Text("Tạo yêu cầu",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.w600,
                                          fontStyle: FontStyle.normal,
                                          fontFamily: 'Roboto',
                                          letterSpacing: 1.0,
                                        )))
                              ],
                            ),
                          ),
                        ),
                      ),

                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height - 240.6,
            decoration: BoxDecoration(
              color: Colors.grey.shade100, // Màu nền của container
            ),
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white, // Màu nền của container
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0),
                        topRight: Radius.circular(20.0)), // Độ bo tròn góc
                  ),
                  margin: EdgeInsets.only(left: 10, right: 10),
                  height: 120,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 120,
                        width: 100,
                        child: Column(
                          children: [
                            Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage('asset/image/waterclock.jpg'),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5),
                              height: 55,
                              child: Text("Lắp đặt\nđồng hồ",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 13.0,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: 'Roboto',
                                    letterSpacing: 1.0,
                                  )),
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: 120,
                        width: 140,
                        child: Column(
                          children: [
                            Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage('asset/image/pipe.jpg'),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5),
                              height: 55,
                              child: Text(
                                  textAlign: TextAlign.center,
                                  "Di rời\nthay đổi đường ống",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 13.0,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: 'Roboto',
                                    letterSpacing: 1.0,
                                  )),
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: 120,
                        width: 100,
                        child: Column(
                          children: [
                            Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage('asset/image/giotnuoc.jpg'),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5),
                              height: 55,
                              child: Text("Kiểm tra kiểm\nđịnh đồng hồ",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 13.0,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: 'Roboto',
                                    letterSpacing: 1.0,
                                  )),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 10, right: 10),
                  height: 120,
                  color: Colors.white,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        height: 120,
                        width: 170,
                        child: Column(
                          children: [
                            Container(
                              height: 60,
                              width: 115,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage('asset/image/lock.png'),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5),
                              height: 55,
                              child: Text(
                                  textAlign: TextAlign.center,
                                  "Đề nghị tạm ngừng ,\nmở cấp nước",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 13.0,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: 'Roboto',
                                    letterSpacing: 1.0,
                                  )),
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: 120,
                        width: 170,
                        child: Column(
                          children: [
                            Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage('asset/image/contract.png'),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5),
                              height: 55,
                              child: Text(
                                  textAlign: TextAlign.center,
                                  "Thay đổi thông tin\nký lại hợp đồng",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 13.0,
                                    fontWeight: FontWeight.bold,
                                    fontStyle: FontStyle.normal,
                                    fontFamily: 'Roboto',
                                    letterSpacing: 1.0,
                                  )),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                    height: 350,
                    width: 350,
                    child: ListView.builder(
                      itemCount: list.length,
                      itemBuilder: (context, index) => Column(
                        children: [
                          TicketCard(op: list[index]),
                          SizedBox(
                            height: 10,
                          )
                        ],
                      ),
                    ))
              ],
            ),
          )

        ],
      ) : FormScreen()
    ) : Scaffold(
        body: Center(
          child: LoadingAnimationWidget.staggeredDotsWave(
            color: Colors.black,
            size: 200,
          ),
        ));

  }
}
