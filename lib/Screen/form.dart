import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:water/api/apis.dart';
import 'package:water/models/request.dart';
import 'package:water/provider/user.dart';

class FormScreen extends StatefulWidget {
  const FormScreen({super.key});

  @override
  State<FormScreen> createState() => _FormScreenState();
}

class _FormScreenState extends State<FormScreen> {
  final TextEditingController title = TextEditingController();
  final TextEditingController content = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<UserProvider>(context);
    return Container(
          color: Colors.white,
          child:Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Text(
                    "Gửi Yêu Cầu Xử Lý",
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w500,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment:
                  MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: 15),
                          child: Text(
                            "Tiêu đề báo cáo",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                        Text(
                          " *",
                          style: TextStyle(color: Colors.red),
                        )
                      ],
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.all(8.0),
                    //   child: Container(
                    //     height: 30,
                    //     width: 50,
                    //     decoration: BoxDecoration(
                    //         color: Colors.grey.shade100,
                    //         border: Border.all(
                    //           color: Colors.blue.shade400, // Màu của viền
                    //         ),
                    //         borderRadius: BorderRadius.all(Radius.circular(10))
                    //     ),
                    //     margin: EdgeInsets.only(right: 20),
                    //     child: Container(
                    //         alignment: Alignment.center,
                    //         child: Text("Chọn", textAlign: TextAlign.center,
                    //           style: TextStyle(color: Colors.black38),
                    //         )),
                    //   ),
                    // )
                  ],
                ),
              ),
              TextFormField(
                controller: title,
                maxLines: 1,
                decoration: InputDecoration(
                  hintText: 'Tiêu đề báo cáo',
                  hintStyle: TextStyle(
                    fontSize: 16.0,
                    color: Colors.grey,
                  ),
                  contentPadding:
                  EdgeInsets.symmetric(horizontal: 15.0),
                  // Độ cao của TextFormField
                  border: InputBorder.none, // Tắt viền
                ),
              ),
              Center(
                child: Container(
                  width: 300,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black26),
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 15),
                        child: Text(
                          "Nội dung báo cáo",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Text(
                        " *",
                        style: TextStyle(color: Colors.red),
                      )
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              TextFormField(
                controller: content,
                maxLines: 2,
                decoration: InputDecoration(
                  hintText: 'Nội dung báo cáo',
                  hintStyle: TextStyle(
                    fontSize: 16.0,
                    color: Colors.grey,
                  ),
                  contentPadding:
                  EdgeInsets.symmetric(horizontal: 15.0),
                  // Độ cao của TextFormField
                  border: InputBorder.none, // Tắt viền
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius:
                    BorderRadius.all(Radius.circular(12))),
                child: DottedBorder(
                  color: Colors.white,
                  borderType: BorderType.RRect,
                  radius: Radius.circular(12),
                  padding: EdgeInsets.all(6),
                  child: ClipRRect(
                    borderRadius:
                    BorderRadius.all(Radius.circular(12)),
                    child: Center(
                      child: Container(
                        color: Colors.red,
                        height: 200,
                        width: MediaQuery.of(context).size.width,
                        child: ImagePickerExample(),
                        // child: Container(
                        //   alignment: Alignment.center,
                        //   child: Text("Thêm ảnh", textAlign: TextAlign.center),
                        // ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Center(
                child: Container(
                  width: 300,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black26),
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Center(
                child: Container(
                  margin: EdgeInsets.only(top: 5),
                  height: 60,
                  width: 150,
                  child: Center(
                      child: ElevatedButton(
                        onPressed: () async {
                          print('có nhấn nút');
                          Request log = new Request(
                            createdDate: DateTime.now().toString() ,
                            note: content.value.text,
                            ticketTypeId: 1,
                            ticketStatusId: 1 ,
                            levelId: 1,
                            customerId: provider.Id,
                            branchId: provider.Brand,
                            title: title.value.text

                          );
                          if( await createRequest(log)){
                            print('Thành công tạo log');
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          primary: Colors.white, // Màu nền của nút
                          onPrimary: Colors.white, // Màu chữ của nút
                          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0), // Bo tròn góc
                          ),
                          elevation: 3, // Độ nổi của nút
                        ),
                        child: Text(
                          'Gửi Yêu Cầu',
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.black
                          ),
                        ),
                      )
                  ),
                ),
              ),
            ],
          ),
        );

  }


}
class ImagePickerExample extends StatefulWidget {
  @override
  _ImagePickerExampleState createState() => _ImagePickerExampleState();
}

class _ImagePickerExampleState extends State<ImagePickerExample> {
  XFile? _imageFile;

  // Hàm mở thư viện ảnh và chọn ảnh
  Future<void> _pickImage() async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      setState(() {
        _imageFile = pickedFile;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _imageFile == null
                ? Text('Không có ảnh được chọn')
                : Container(
                height: 130,
                width: 250,
                child: Image.file(File(_imageFile!.path))),
            SizedBox(height: 10),
            ElevatedButton(
              onPressed: _pickImage,
              child: Text('Chọn ảnh'),
            ),
          ],
        ),
      ),
    );
  }
}