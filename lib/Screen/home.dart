
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:water/Widget/Recommendation.dart';
import 'package:water/provider/user.dart';
class Home extends StatefulWidget {
  final VoidCallback callback;
  const Home({super.key, required this.callback});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<UserProvider>(context);
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: Color(0xFF2CB4DC)
              // image: DecorationImage(
              //   image: AssetImage('asset/image/blue.jpg'),
              //   // Đường dẫn đến hình ảnh
              //   fit: BoxFit.fill,
              // ),
            ),
            child: Column(
              children: [
                Container(
                  height: 70,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Container(
                            margin: EdgeInsets.only(left: 20),
                            child: Text(
                              "Hi,",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 30.0,
                                fontWeight: FontWeight.bold,
                                fontStyle: FontStyle.normal,
                                fontFamily: 'Roboto',
                                letterSpacing: 1.0,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 5),
                            child: Text(
                              provider.Name!,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                fontStyle: FontStyle.normal,
                                fontFamily: 'Roboto',
                                letterSpacing: 1.0,
                              ),
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            child: Icon(
                              Icons.search,
                              size: 30,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 15),
                            child: Icon(
                              Icons.notifications,
                              size: 30,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                    decoration: BoxDecoration(
                      color: Colors.grey.shade100, // Màu nền của container
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20.0),
                          topRight: Radius.circular(20.0)), // Độ bo tròn góc
                    ),
                    height: MediaQuery.sizeOf(context).height - 94 - 6.6 - 60,
                    child: ListView(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 20),
                              child: Text(
                                "Đang xem hợp đồng",
                                style: TextStyle(
                                  fontSize: 12,
                                  // Tăng kích thước văn bản để dễ đọc hơn trên thiết bị di động
                                  fontWeight: FontWeight.normal,
                                  // In đậm văn bản để nó nổi bật hơn
                                  color: Colors.black54,
                                  // Màu văn bản là màu đen
                                  fontFamily:
                                  'Roboto', // Sử dụng font chữ Roboto để phù hợp với giao diện mặc định của Flutter
                                ),
                              ),
                            ),
                            Row(
                              children: [
                                Container(
                                    margin: EdgeInsets.only(left: 20),
                                    child: Text(
                                      "SS14F23DSR",
                                      style: TextStyle(
                                        fontSize: 16,
                                        // Tăng kích thước văn bản để dễ đọc hơn trên thiết bị di động
                                        fontWeight: FontWeight.bold,
                                        // In đậm văn bản để nó nổi bật hơn
                                        color: Colors.black87,
                                        // Màu văn bản là màu đen
                                        fontFamily:
                                        'Roboto', // Sử dụng font chữ Roboto để phù hợp với giao diện mặc định của Flutter
                                      ),
                                    )),
                                Icon(
                                  Icons.arrow_drop_down,
                                  size: 30,
                                ),
                                Container(
                                  padding: EdgeInsets.all(5.0),
                                  decoration: BoxDecoration(
                                    color: Colors.blue.withOpacity(0.2),
                                    // Màu nền hơi mờ
                                    borderRadius: BorderRadius.circular(30.0),
                                    // Bo tròn 4 góc
                                    border: Border.all(
                                      color: Colors.blue.shade900,
                                      // Màu đường viền xanh dương
                                      width: 1.0, // Độ dày của đường viền
                                    ),
                                  ),
                                  child: Text(
                                    "Hợp đồng chính chủ",
                                    style: TextStyle(
                                      fontSize: 12,
                                      // Tăng kích thước văn bản để dễ đọc hơn trên thiết bị di động
                                      fontWeight: FontWeight.normal,
                                      // In đậm văn bản để nó nổi bật hơn
                                      color: Colors.blue[900],
                                      // Màu văn bản là màu đen
                                      fontFamily:
                                      'Roboto', // Sử dụng font chữ Roboto để phù hợp với giao diện mặc định của Flutter
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                                height: 120,
                                width: MediaQuery.of(context).size.width,
                                child: Container(
                                  padding: EdgeInsets.only(right: 20, left: 20),
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      InkWell(
                                        onTap: (){
                                          widget.callback();
                                        },
                                        child: Container(
                                          height: 95,
                                          width: 72,
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                            BorderRadius.circular(10.0),
                                            // Đặt bán kính bo tròn cho 4 góc
                                            border: Border.all(
                                                color: Colors
                                                    .transparent), // Loại bỏ viền
                                          ),
                                          child: Column(
                                            mainAxisAlignment:
                                            MainAxisAlignment.center,
                                            children: [
                                              Stack(
                                                children: [
                                                  Container(
                                                    height: 50,
                                                    width: 50,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                        image: AssetImage('asset/image/blue.jpg'),
                                                        // Đường dẫn đến hình ảnh
                                                        fit: BoxFit.fill,
                                                      ),
                                                      borderRadius:
                                                      BorderRadius.circular(
                                                          9),
                                                    ),
                                                  ),
                                                  Positioned(
                                                    top: 5,
                                                    right: 5,
                                                    bottom: 5,
                                                    left: 5,
                                                    child: Container(
                                                      height: 35,
                                                      width: 35,
                                                      decoration: BoxDecoration(
                                                        image: DecorationImage(
                                                          image: AssetImage('asset/image/contract.png'),
                                                          // Đường dẫn đến hình ảnh
                                                          fit: BoxFit.fill,
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              ),
                                              Container(
                                                margin: EdgeInsets.only(top: 3),
                                                child: Text(
                                                  "Quản lý\nhợp đồng",
                                                  style: TextStyle(
                                                    fontSize: 12,
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.w700,
                                                    fontFamily: 'Roboto',
                                                  ),
                                                  textAlign: TextAlign.center,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                        height: 95,
                                        width: 72,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                          BorderRadius.circular(10.0),
                                          // Đặt bán kính bo tròn cho 4 góc
                                          border: Border.all(
                                              color: Colors
                                                  .transparent), // Loại bỏ viền
                                        ),
                                        child: Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: [
                                            Stack(
                                              children: [
                                                Container(
                                                  height: 50,
                                                  width: 50,
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image: AssetImage('asset/image/blue.jpg'),
                                                      // Đường dẫn đến hình ảnh
                                                      fit: BoxFit.fill,
                                                    ),
                                                    borderRadius:
                                                    BorderRadius.circular(
                                                        9),
                                                  ),
                                                ),
                                                Positioned(
                                                  top: 5,
                                                  right: 5,
                                                  bottom: 5,
                                                  left: 5,
                                                  child: Container(
                                                    height: 40,
                                                    width: 35,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                        image: AssetImage('asset/image/drop.png'),
                                                        // Đường dẫn đến hình ảnh
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 3),
                                              child: Text(
                                                "Quản lý\nnước",
                                                style:
                                                TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w700,
                                                  fontFamily: 'Roboto',
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Container(
                                        height: 95,
                                        width: 72,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                          BorderRadius.circular(10.0),
                                          // Đặt bán kính bo tròn cho 4 góc
                                          border: Border.all(
                                              color: Colors
                                                  .transparent), // Loại bỏ viền
                                        ),
                                        child: Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: [
                                            Stack(
                                              children: [
                                                Container(
                                                  height: 50,
                                                  width: 50,
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image: AssetImage('asset/image/blue.jpg'),
                                                      // Đường dẫn đến hình ảnh
                                                      fit: BoxFit.fill,
                                                    ),
                                                    borderRadius:
                                                    BorderRadius.circular(
                                                        9),
                                                  ),
                                                ),
                                                Positioned(
                                                  top: 5,
                                                  right: 5,
                                                  bottom: 5,
                                                  left: 5,
                                                  child: Container(
                                                    height: 40,
                                                    width: 35,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                        image: AssetImage('asset/image/internet.png'),
                                                        // Đường dẫn đến hình ảnh
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 3),
                                              child: Text(
                                                "Giao dịch\ntrực tuyến",
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w700,
                                                  fontFamily: 'Roboto',
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      Container(
                                        height: 95,
                                        width: 72,
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                          BorderRadius.circular(10.0),
                                          // Đặt bán kính bo tròn cho 4 góc
                                          border: Border.all(
                                              color: Colors
                                                  .transparent), // Loại bỏ viền
                                        ),
                                        child: Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: [
                                            Stack(
                                              children: [
                                                Container(
                                                  height: 50,
                                                  width: 50,
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image: AssetImage('asset/image/blue.jpg'),
                                                      // Đường dẫn đến hình ảnh
                                                      fit: BoxFit.fill,
                                                    ),
                                                    borderRadius:
                                                    BorderRadius.circular(
                                                        9),
                                                  ),
                                                ),
                                                Positioned(
                                                  top: 5,
                                                  right: 5,
                                                  bottom: 5,
                                                  left: 5,
                                                  child: Container(
                                                    height: 30,
                                                    width: 30,
                                                    decoration: BoxDecoration(
                                                      image: DecorationImage(
                                                        image: AssetImage('asset/image/more.png'),
                                                        // Đường dẫn đến hình ảnh
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top: 3),
                                              child: Text(
                                                "Dịch vụ\nkhác",
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w700,
                                                  fontFamily: 'Roboto',
                                                ),
                                                textAlign: TextAlign.center,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                )),
                            Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 10, vertical: 5),
                                child: Container(
                                  width: 370,
                                  height: 100,
                                  child: ClipRRect(
                                    child: Image.asset(
                                      "asset/image/water.jpeg",
                                      fit: BoxFit.cover,
                                    ),
                                    borderRadius: BorderRadius.circular(20.0),
                                  ),
                                )),
                            Recommendation(),
                            Recommendation()
                          ],
                        )
                      ],
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
